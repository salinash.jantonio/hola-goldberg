#!/usr/bin/env python
# -*- coding: utf-8 -*-

import libs.placaBase as pB
import time
import libs.webDB as DBweb
import libs.pantalla.pantalla_ctrl as pant
import libs.read_db as TFdb
import os
import serial
poema_motor = ['AL MOTOR MARAVILLOSO - Juan Parra del Riego (Perú, 1894-1925)',
        'Yo que canté un día la belleza violenta y la alegría de las locomotoras y de los aeroplanos,',
        'qué serpentina loca le lanzaré hoy al mundo para cantar tu arcano, tus vivos cilindros sonámbulos, tu fuego profundo.',
        '¡Oh, tú el motor oculto de mi alma y de mis manos!',
        '¡Qué llama enloquecida se enreda en tus fogones y que hace girar la rueda líquida de la sangre',
        'y atiranta las poleas de los músculos para mecer los columpios súbitos de las sensaciones,',
        'cuando corro, beso anhelo, callo, sufro, espero, miro,',
        'salta mi alma en una loca carcajada, floto en sedas de suspiro o en el charco solitario de la sombra en que me estiro',
        'se me copia el corazón como una estrella desolada!',
        'Y que electricidades se me van por los alambres calientes de los nervios hasta el cerebro,',
        'caja de las velocidades, azules y negras y rejas de todos los sueños...',
        'Zumba la turbina sutil de hondos dolores y saltan imágenes,',
        'y hacia donde ya no alcanza el ojo triste con sus sedientas ruedas de colores corre el tren de las imágenes...',
        'Y qué émbolos oscuros se agitan si cesar, y que carbón jadeante de soles escondidosa todo vapor, a todo vapor,',
        'te hace andar cuando se me hincha el corazón de una salvaje alegría o se me quiere romper el dolor y de melancolía',
        'Motor humano: tú eres la única maravilla de este mundo doloroso, por tu inmortal prodigio: el beso de las mujeres,',
        'el pensamiento firme y armonioso, la palabra que salta rotunda,',
        'patética y viva por la célula furtiva que trabaja en sus telares nuestro ritmo misterioso;',
        'teje un día la Esperanza, otro día la alegría.',
        'Yo siento  cuando queda tensa y viva sobre mi alma la Energía,',
        '¡Motor de la explosión de toda la vida mía!',
        'mi callada pasión y mi fuerza y mi canto, más ligero, más ligero,',
        'con la carga de esperanza que es mi única conquista: tú, la máquina del único sendero sin sendero;',
        'yo, tu alado y sangriento maquinista.'
        ]

#try:
#	ser = serial.Serial('/dev/ttyUSB0',9600)
#except:
#	pass

def tiempo(t):
	count=0
	while (count < (t*10)):
		a = DBweb.st_db()
        	a= int(a)
		if a != aux_exit :
			break
                time.sleep(.1)
                count=count+1
def clicliGB_Tiempo():
	pB.motor2('ENCENDIDO')
       	pB.motor1('ENCENDIDO')
	#tiempo(20)
	time.sleep(20)
	
	pB.motor2('APAGADO')
        pB.motor1('APAGADO')
	time.sleep(10)
	#tiempo(10)
			
def TF_Ciclo():
	INFOapp=TFdb.ret_APPjuego()
	if INFOapp == 50:
		#pantalla.send('#HolaGoldberg Actívame!')
 		RS , INFO = TFdb.ret_un_elem()
		if RS=='Facebook':
        		#try:
			#	serie.write('1')
			#except:
			#	pass
			txt= 'Gracias ya tenemos ' + str(INFO) + ' likes'
                	pantalla.send(txt)
                	clicliGB_Tiempo()
                	tiempo(3)
                	pantalla.send('#HolaGoldberg Actívame!')
     		elif RS=='Twitter':
			#try:
			#	serie.write('1')
                	#except:
			#	pass
			txt= 'Hola @' + str(INFO)
                	pantalla.send(txt)
                	clicliGB_Tiempo()
                	tiempo(3)
                	pantalla.send('#HolaGoldberg Actívame!!')
	else:
		if INFOapp == 51:
			#pantalla.send(" ")
			#time.sleep(1)
			txt= '¡Incorrecto! =('
                        pantalla.send(txt)
			time.sleep(12)
			#tiempo(12)
			pantalla.send('#HolaGoldberg Actívame!!')
			time.sleep(3)
			aux=TFdb.ret_APPjuego()
			
		else: 
			#pantalla.send(" ")
			#time.sleep(1)
			txt=poema_motor[INFOapp]
			pantalla.send(txt)
			clicliGB_Tiempo()
			time.sleep(10)
			#tiempo (10)
			pantalla.send('#HolaGoldberg Actívame!!')
			time.sleep(3)
			aux=TFdb.ret_APPjuego()
while 1:
	try:
		#pantalla = pant.pantalla_class('/dev/ttyS0')
		pantalla = pant.pantalla_class('/dev/ttyACM0')
		time.sleep(3);
		pantalla.send('#HolaGoldberg Actívame!!')
		RS , INFO = TFdb.ret_un_elem()
	
		pB.configurar()
		pB.motor1('APAGADO')
		pB.motor2('APAGADO')
		pB.luz('APAGADO')
		pB.buzzer('APAGADO')

		pB.luz('ENCENDIDO')
		aux1=0
		aux2=0
		aux3=0
		aux4=0
		aux5=0
		aux_exit=0
		while 1:
			a = DBweb.st_db()
			a= int(a)
			#print a
			if a==1 : 
				aux_exit = 1 
				#print "uno"
				if aux1==0:
					pantalla.send('#HolaGoldberg Actívame!!')
					aux1=1
					aux2=0
					aux3=0
					aux4=0
					aux5=0
				TF_Ciclo()
			elif a==2 :
				aux_exit = 2
				#print "dos"
				if aux2==0:
					pantalla.send('#HolaGoldberg prueba')
                        		aux1=0
                        		aux2=1
                        		aux3=0
                        		aux4=0
                        		aux5=0
				clicliGB_Tiempo()
			elif a==3 :
				aux_exit = 3
				if aux3==0:
					pantalla.send('Detener')
					aux1=0
	                		aux2=0
        	        		aux3=1
                        		aux4=0
                       			aux5=0
					pB.motor1('APAGADO')
					pB.motor2('APAGADO')
			elif a==4 :
				aux_exit = 4 
				pantalla.send('Reiniciando....')
				tiempo(5)
				a = DBweb.st_db()
        			a= int(a)
				if a==aux_exit :
					DBweb.rs_db()
					time.sleep(2) 
					os.system('sudo reboot')
				else:
					aux1=0
                        		aux2=0
                        		aux3=0
                        		aux4=0
					
	except Exception as ex:
		template = "An exception of type {0} occurred. Arguments:\n{1!r}"
		message = template.format(type(ex).__name__, ex.args)
		print message
