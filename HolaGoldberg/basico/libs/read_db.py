# -*- coding: utf-8 -*-

import MySQLdb
import time
# import serial
# import pantalla.pantalla_ctrl as screen 

def run_query(query=''):
	#Datos base de datos
	Host = "####"
	User = "####"
	Pass = "####"
	Database = "redes_sociales"
	conn = MySQLdb.connect(Host, User, Pass, Database, charset='utf8',use_unicode=True)
	cursor = conn.cursor()
        cursor.execute(query)

        if query.upper().startswith('SELECT'):
                data = cursor.fetchall()
        else:
                conn.commit()
                data = None

        cursor.close()
        conn.close

        return data

def ret_un_elem():
	sel = 'SELECT id, red_social, nLike_or_name FROM datos WHERE activado=0 LIMIT 1'
	dato = run_query(sel)
	r_s = None
	like_or_name = None
	if dato:
		ID = dato[0][0]
		r_s = dato[0][1]
		like_or_name = dato[0][2]
		upd = 'UPDATE datos SET activado=1 WHERE id=%d' %ID
		run_query(upd)
	return r_s, like_or_name

def ret_APPjuego():
        sel = 'SELECT id, estado FROM APPjuego WHERE id=1'
        dato = run_query(sel)
	estado= int(dato[0][1])
	if estado != 50 :
		upd = 'UPDATE APPjuego SET estado="50" WHERE id=1'
                run_query(upd)
	return int(estado)

def snd_serial(pto_serial, snd):
	pto_serial.write(snd)
	
try:
	if __name__ == '__main__':
		while 1:
			a = ret_APPjuego()
			print a
			time.sleep(2)
except KeyboardInterrupt:
        print "Salida por teclado"
	ser.close()
        h_break = False
