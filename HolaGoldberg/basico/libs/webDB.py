# -*- coding: utf-8 -*-

import MySQLdb
import time

def run_query(query=''):
	#Datos base de datos
	Host = "####"
	User = "####"
	Pass = "####"
	Database = "m_goldberg"
	conn = MySQLdb.connect(Host, User, Pass, Database, charset='utf8',use_unicode=True)
	cursor = conn.cursor()
        cursor.execute(query)

        if query.upper().startswith('SELECT'):
                data = cursor.fetchall()
        else:
                conn.commit()
                data = None

        cursor.close()
        conn.close

        return data

def st_db():
	sel = 'SELECT on_off FROM goldberg WHERE id = 1'
	dato = run_query(sel)
	return dato[0][0]
def rs_db():
	sel = "UPDATE goldberg SET on_off='1' WHERE id=1"
	dato = run_query(sel)

try:
	if __name__ == '__main__':
		while 1:
			print st_db()
			time.sleep(1)		

except KeyboardInterrupt:
        print "Salida por teclado"
