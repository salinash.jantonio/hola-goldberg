# -*- coding: utf-8 -*-

import serial

class pantalla_class:
	texto_str = " "
	texto_int = 0
	serie = " "

	def __init__(self, nombre_serial):
		self.serie = serial.Serial(port=nombre_serial, baudrate=115200)
		self.serie.isOpen()		

	def ascii_ext(self, car):
		if ord(car) >= 161:
                	val = ord(car) -32
		else:
                	val = ord(car)
        	return val


	def str_ext(self, cad):
		lis_val = []
        	aux = []
        	lis_fin = ''
        	pre = False
        	for x in range(len(cad)):
                	lis_val.append(self.ascii_ext(cad[x]))

        	for y in lis_val:
                	lis_fin += str(y).zfill(3)

        	return lis_fin + '\n'
	
	def send(self, msg):
		self.text_str = unicode(msg, "utf-8")
		self.text_int = self.str_ext(self.text_str)
                self.serie.write(self.text_int)
                return self.text_int

	def clear(self):
		self.text_str = " "
                self.text_int = self.str_ext(self.text_str)
                self.serie.write(self.text_int)
	
	def ctrl_led(self, n_led, status):
		cmd = '00' + str(2 * n_led + status) + '\n'
		print len(cmd)
		self.serie.write(cmd)
	
	def prueba(self):
		self.serie.write('064064064064\n')

