# -*- coding: utf-8 -*-

import json
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import threading
import MySQLdb
import time
import fb.fb_info as fb_data
import libs.webDB as DBweb
import socket

#Claves para accesar a scripts de Twitter
access_token = "#####"
access_token_secret = "#####"
consumer_key = "#####"
consumer_secret = "#####"

#Claves para accesar a scripts de Facebook
id_ccd = "#####"
token = "#####"

REMOTE_SERVER = "www.google.com"

def is_connected():
    try:
        host = socket.gethostbyname(REMOTE_SERVER)
        s = socket.create_connection((host, 80), 2)
        return True
    except:
        pass
    return False

def run_query(query=''):
	#Datos base de datos
	Host = "#####"
	User = "#####"
	Pass = "#####""
	Database = "redes_sociales"
	conn = MySQLdb.connect(Host, User, Pass, Database, charset='utf8',use_unicode=True)
	cursor = conn.cursor()
        cursor.execute(query)

        if query.upper().startswith('SELECT'):
                data = cursor.fetchall()
        else:
                conn.commit()
                data = None

        cursor.close()
        conn.close

        return data

def ins_fb(likes):
	a = DBweb.st_db()
	a = int(a)
	print a
	if a==1 :
		# BD redes_sociales -> Tabla datos
		ins =  "INSERT INTO datos VALUES (NULL, 'Facebook', '%s', 0)" %str(likes)
		run_query(ins)
		# BD redes_sociales -> Tabla activaciones
		ins = "INSERT INTO activaciones VALUES (NULL, 'Facebook', '%s', NULL)" %str(likes)
		run_query(ins) 

def ins_tw(Screen_Name):
	a = DBweb.st_db()
	a = int(a)
	print a
	if a==1 :
		# BD redes_sociales -> Tabla datos
		ins =  "INSERT INTO datos VALUES(NULL, 'Twitter', '%s', 0)" %Screen_Name
        	run_query(ins)
		# BD redes_sociales -> Tabla activaciones
                ins = "INSERT INTO activaciones VALUES (NULL, 'Twitter', '%s', NULL)" %Screen_Name
                run_query(ins)

def fb_likes(page_fb):
	while True:
		try:
			#Obtiene no. de likes actual por primera vez
			cu_fan = page_fb.get_fan_count()
			n_valor = cu_fan
			print cu_fan
			while True:
				#Obtiene no. de likes nuevo
				n_valor = page_fb.get_fan_count()
				#compara no. de likes actual con no. de likes anterior
				if cu_fan != n_valor:
					print "-"*57
                                	print "Actual %d" %cu_fan
                                	print "Nuevo %d" %n_valor
                                	print "-"*57
					if cu_fan < n_valor:
						print "Nuevo Like"
						for nu in range(cu_fan + 1, n_valor + 1):
							print "Like No.: %d" %nu
							# Funcion que agrega dato de facebook a base de datos	
							ins_fb(nu)
						# Actualiza el no. de likes nuevo(n_valor) al actual(cu_fan)
					cu_fan = n_valor
				time.sleep(3)
		
		except Exception as ex:
			template = "An exception of type {0} occured. Arguments:\n{1!r}"
			message = template.format(type(ex).__name__, ex.args)
			print message	

#Clase para Stream de Twitter
class StdOutListener(StreamListener):
	
	def on_data(self, data):
		a = json.loads(data)
		b = a['user']
		print 'Tweet de %s' %b['screen_name']
		#Funcion que agrega dato de facebook a base de datos
		ins_tw(b['screen_name'])
		return True

	def on_error(self, status):
		print "Error Twitter"
		print status
		return True

#Funcion main
try:
	if __name__ == '__main__':
		
		print(is_connected())
		while is_connected() is False:
			print 'No hay internet'
            		time.sleep(1)
		
		# ccd = fb_data.fb_info(id_ccd, token)
		
		# f = threading.Thread(target=fb_likes, args=(ccd,))
		# f.start()
		
		l = StdOutListener()
		auth = OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)
	
		stream = Stream(auth, l)
		stream.filter(track=['"#####"'], async=True)

except Exception as ex:
	template = "An exception of type {0} occured. Arguments:\n{1!r}"
	message = template.format(type(ex).__name__, ex.args)
	print message
