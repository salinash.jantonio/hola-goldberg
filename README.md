<h1>Hola Goldberg</h1>
              <h5>
                ¡Hola Goldberg! Es una máquina que complejiza una tarea sencilla (desplazar una esfera de un punto a otro) haciendo uso de mecanismos inspirados en los inventos de Rube Goldberg (1883-1970. EEUU).
              </h5>
              <h5>
                La máquina es activada cuando en Twitter se utilizan una serie de hashtags previamente definidos, que detonan el movimiento de los distintos motores que generan la trayectoria de la esfera.
              </h5>
              <h5>
                Con ¡Hola Goldberg! se busca crear un puente entre las tecnologías de las máquinas mecánicas, la programación y la escritura en soportes digitales.
              </h5>
              <h5>
                Trabajo realizado para el Centro de Cultura Digital en colaboración de Salvador Chávez y Francisco Valencia. Actualmente se encuentra exhibida en las instalaciones de La Colmena Centro de Tecnologías Creativas Grace Quintanilla ubicado en Tlaxcala de Xicohténcatl, Tlaxcala.
              </h5>